#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "common.h"

static struct coord coords[MAX_POTENTIAL + 3];
static struct coord *head = coords + 0;
static struct coord *tail = coords + 1;
static struct coord *visited = coords + 2;
static struct coord *visited_p = coords + 3;

void
head_tail_fix_distance(void)
{
    chain_fix_distance(head, tail);
}

void
move_head(struct vec const *move)
{
    int value = move->value;
    int *targets[DIR_END] = { &(head->y), &(head->x), &(head->y), &(head->x) };
    int polarities[DIR_END] = { -1, +1, +1, -1 };

    while (value) {
        printf("Move %s tail (%i, %i) head (%i, %i)\n", direction_str[move->dir], tail->x, tail->y, head->x, head->y);
        *(targets[move->dir]) += polarities[move->dir];
        head_tail_fix_distance();
        visited_push_tail_unique();
        --value;
    }
}

int
main(void)
{
    if (!data_populate())
        return 1;

    for (int i = 0; i < 2000; ++i)
        move_head(data + i);

    printf("number of unique points visited: %zu\n", visited_get_len());

    return 0;
}
