#include "input.h"

#define MAX_POTENTIAL 11450
// see get_max_potential()

enum direction {
    DIR_UP,
    DIR_RIGHT,
    DIR_DOWN,
    DIR_LEFT,
    DIR_END,
};

struct coord {
    int x;
    int y;
};

struct vec {
    enum direction dir;
    int value;
};

static struct coord coords[];
static struct coord *head;
static struct coord *tail;
static struct coord *visited;
static struct coord *visited_p;

static char const *direction_str[DIR_END] = {
    "Up",
    "Right",
    "Down",
    "Left",
};

static char const *direction_chr = "URDL";

static struct vec data[2000];

static inline size_t
visited_get_len(void)
{
    return visited_p - visited;
}

void
visited_push_unique(struct coord const *coord)
{
    visited_p->x = coord->x;
    visited_p->y = coord->y;
    ++visited_p;
}

int
visited_push_tail_unique(void)
{
    size_t visited_len = visited_get_len();

    for (size_t i = 0; i < visited_len; ++i)
        if (visited[i].x == tail->x && visited[i].y == tail->y)
            return 0;

    visited_push_unique(tail);
    return 1;
}

/*
 * move follower to follow leader according to the extraordinarily
 * overcomplicated rules of the challenge
 *
 * 1. diagonal movement (the overcomplicated part)
 * 2. simple orthogonal movement
 */
void
chain_fix_distance(struct coord const *leader, struct coord *follower)
{
    int xdiff = leader->x - follower->x;
    int ydiff = leader->y - follower->y;
    int xabs = xdiff;
    int yabs = ydiff;

    if (xdiff < 0)
        xabs = -xabs;

    if (ydiff < 0)
        yabs = -yabs;

    if (xabs > 1 && yabs == 1) { // 1
        if (xdiff < -1)
            follower->x -= 1;

        if (xdiff > 1)
            follower->x += 1;

        if (ydiff == -1)
            follower->y -= 1;

        if (ydiff == 1)
            follower->y += 1;

        return;
    }

    if (yabs > 1 && xabs == 1) { // 1
        if (ydiff < -1)
            follower->y -= 1;

        if (ydiff > 1)
            follower->y += 1;

        if (xdiff == -1)
            follower->x -= 1;

        if (xdiff == 1)
            follower->x += 1;

        return;
    }

    // 2
    if (xdiff < -1)
        follower->x -= 1;

    if (xdiff > 1)
        follower->x += 1;

    if (ydiff < -1)
        follower->y -= 1;

    if (ydiff > 1)
        follower->y += 1;
}

int
data_populate(void)
{
    for (int i = 0; i < 2000; ++i) {
        char const *line = lines[i];
        char const *match = strchr(direction_chr, line[0]);
        struct vec *node = data + i;

        if (match == NULL) {
            printf("E: invalid direction char %02x '%c'\n", line[0], line[0]);
            return 0;
        }

        node->dir = match - direction_chr;
        node->value = atoi(line + 1);
    }

    return 1;
}

// provided for documentation. This is how the figure of 11450 was
// calculated
static inline int
get_max_potential(void)
{
    int total = 0;

    for (int i = 0; i < 2000; ++i)
        total += data[i].value;

    return total;
}

