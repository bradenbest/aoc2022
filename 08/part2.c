#include <stdio.h>

#include "common.h"

#define MAX(a, b) \
    (((a) > (b)) ? (a) : (b))

// these functions check how many trees are visible in the given
// direction before the view is blocked or edge of grid
int
view_up(int x, int y)
{
    int total = 0;

    for (int y2 = y - 1; y2 >= 0; --y2) {
        ++total;

        if (lines[y2][x] >= lines[y][x])
            break;
    }

    return total;
}

int
view_down(int x, int y)
{
    int total = 0;

    for (int y2 = y + 1; y2 < GRIDH; ++y2) {
        ++total;

        if (lines[y2][x] >= lines[y][x])
            break;
    }

    return total;
}

int
view_left(int x, int y)
{
    int total = 0;

    for (int x2 = x - 1; x2 >= 0; --x2) {
        ++total;

        if (lines[y][x2] >= lines[y][x])
            break;
    }

    return total;
}

int
view_right(int x, int y)
{
    int total = 0;

    for (int x2 = x + 1; x2 < GRIDW; ++x2) {
        ++total;

        if (lines[y][x2] >= lines[y][x])
            break;
    }

    return total;
}

int
get_view_score(int x, int y)
{
    int (*fns[])(int, int) = { view_up, view_right, view_down, view_left, };
    int total = 1;

    for (int i = 0; i < 4; ++i)
        total *= fns[i](x, y);

    return total;
}

int
main(void)
{
    int max = 0;

    for (int y = 0; y < GRIDH; ++y)
        for (int x = 0; x < GRIDW; ++x)
            max = MAX(max, get_view_score(x, y));

    printf("largest score of any tree: %u\n", max);
}
