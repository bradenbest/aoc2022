#include <stdio.h>

#include "common.h"

// These functions check whether a tree is visible from the outside
static inline int
is_tree_visible_edge(int x, int y)
{
    return x == 0
        || x == GRIDW-1
        || y == 0
        || y == GRIDH-1;
}

static inline int
is_tree_visible_up(int x, int y)
{
    for (int y2 = y - 1; y2 >= 0; --y2)
        if (lines[y2][x] >= lines[y][x])
            return 0;

    return 1;
}

static inline int
is_tree_visible_down(int x, int y)
{
    for (int y2 = y + 1; y2 < GRIDH; ++y2)
        if (lines[y2][x] >= lines[y][x])
            return 0;

    return 1;
}

static inline int
is_tree_visible_left(int x, int y)
{
    for (int x2 = x - 1; x2 >= 0; --x2)
        if (lines[y][x2] >= lines[y][x])
            return 0;

    return 1;
}

static inline int
is_tree_visible_right(int x, int y)
{
    for (int x2 = x + 1; x2 < GRIDW; ++x2)
        if (lines[y][x2] >= lines[y][x])
            return 0;

    return 1;
}

int
is_tree_visible(int x, int y)
{
    return is_tree_visible_edge(x, y)
        || is_tree_visible_up(x, y)
        || is_tree_visible_down(x, y)
        || is_tree_visible_left(x, y)
        || is_tree_visible_right(x, y);
}

static inline int
is_tree_visible_log(int x, int y, int (*fn)(int, int), char const *label)
{
    int res = fn(x, y);

    printf("[%u, %u] '%c' %6s: %s\n", x, y, lines[y][x], label, res ? "True" : "False");
    return res;
}

int
main(void)
{
    int total = 0;

    for (int y = 0; y < GRIDH; ++y)
        for (int x = 0; x < GRIDW; ++x)
            total += is_tree_visible(x, y);

    printf("total unique trees visible: %u\n", total);
}
