#include <stdio.h>
#include <string.h>

#include "common.h"

static inline int
get_common(char const *line)
{
    size_t len = strlen(line);

    for (size_t i = 0; i < len / 2; ++i)
        if (strchr(line + (len / 2), line[i]) != NULL)
            return line[i];

    return 0;
}

int
main(void)
{
    int total = 0;

    for (size_t i = 0; i < lines_len; ++i) {
        int common = get_common(lines[i]);
        int priority = get_priority(common);

        printf("line: %s, common: %c, priority = %u\n", lines[i], common, priority);
        total += priority;
    }

    printf("Total: %u\n", total);
    return 0;
}
