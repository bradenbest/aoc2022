#include "input.h"

static inline int
get_priority(int ch)
{
    static char const *priority = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char const *res = strchr(priority, ch);

    return 1 + (res - priority);
}
