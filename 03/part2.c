#include <stdio.h>
#include <string.h>

#include "common.h"

static inline int
get_common(char const *line, char const *line2, char const *line3)
{
    size_t len = strlen(line);

    for (size_t i = 0; i < len; ++i)
        if (strchr(line2, line[i]) != NULL && strchr(line3, line[i]) != NULL)
            return line[i];

    return 0;
}

int
main(void)
{
    int total = 0;

    for (size_t i = 0; i < lines_len; i += 3) {
        int common = get_common(lines[i], lines[i + 1], lines[i + 2]);
        int priority = get_priority(common);

        printf("line: %.5s... / %.5s... / %.5s..., common: %c, priority = %u\n", lines[i], lines[i+1], lines[i+2], common, priority);
        total += priority;
    }

    printf("Total: %u\n", total);
    return 0;
}
