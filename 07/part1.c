#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#include "common.h"

int
main(void)
{
    size_t total = 0;

    load_filesystem();
    //print_filesystem_detail();

    for (size_t i = 0; get_node(i)->type != NT_EMPTY; ++i) {
        size_t directory_size = collect_size(i);

        if (get_node(i)->type == NT_DIR && directory_size <= 100000)
            total += directory_size;
    }

    printf("Sum of directory sizes where size <= 100000: %lu\n", total);
    return 0;
}
