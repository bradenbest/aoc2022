#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#include "common.h"

#define DISKLIMIT 70000000
#define QUOTA     30000000

int
main(void)
{
    size_t used;
    size_t available;
    size_t smallest;

    load_filesystem();

    used = collect_size(0);
    available = DISKLIMIT - used;
    smallest = used;

    printf("Disk:  %u\n", DISKLIMIT);
    printf("Need:  %u\n", QUOTA);
    printf("Avail: %lu\n", available);

    for (size_t i = 0; get_node(i)->type != NT_EMPTY; ++i) {
        size_t directory_size = collect_size(i);

        if (get_node(i)->type != NT_DIR)
            continue;

        if (directory_size < smallest && directory_size >= (QUOTA - available))
            smallest = directory_size;
    }

    printf("Freed %lu bytes of space. New avail = %lu\n", smallest, smallest + available);
    return 0;
}
