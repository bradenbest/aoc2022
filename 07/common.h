#define FSLIMIT 512
#define NAMESZ 40
#define CHILDRENSZ 20
#define CWDSZ 50

#include "input.h"

enum fsnodetype {
    NT_EMPTY,
    NT_FILE,
    NT_DIR,
};

struct fsnode {
    size_t size;
    char name[NAMESZ];
    int children[CHILDRENSZ];
    int nchildren;
    int type;
};

static char *fsnodetype_str[] = {
    "Invalid",
    "Regular File",
    "Directory",
};

static struct fsnode fsroot[FSLIMIT] = {
    {0, "", {}, 0, NT_DIR},
};

static int cwd[CWDSZ];
static size_t cwdlen;

// returns node pointer via ID
struct fsnode *
get_node(int id)
{
    return fsroot + id;
}

// returns the first empty ID or 0 on fail
int
new_id(void)
{
    for (int i = 1; i < FSLIMIT; ++i)
        if (get_node(i)->type == NT_EMPTY)
            return i;

    return 0;
}

// pushes child to parent's children[], returns 1 or 0 (success/fail)
int
dir_push(int parent, int child)
{
    struct fsnode *selnode = fsroot + parent;

    if (selnode->nchildren >= CHILDRENSZ)
        return 0;

    selnode->children[selnode->nchildren++] = child;
    return 1;
}

// pushes child to CWD, returns 1 or 0 (success/fail)
int
cwd_push(int child)
{
    return dir_push(cwd[cwdlen - 1], child);
}

// creates new FS node and returns its ID
// this is distinct from putting it in the tree
int
fs_push(char const *name, size_t size, int type)
{
    int id;
    struct fsnode *selnode;

    if ((id = new_id()) == 0)
        return 0;

    selnode = get_node(id);
    strcpy(selnode->name, name);
    selnode->size = size;
    selnode->nchildren = 0;
    selnode->type = type;
    return id;
}

// takes current line number and reads until '$'. For each line, it
// parses it and adds it to the tree. Returns the new line number so the
// caller can continue
size_t
ls_directory(size_t lineno)
{
    static char linebuf[64];

    for (size_t i = lineno; i < lines_len; ++i) {
        char *tok;
        size_t size = 0;
        int type = NT_DIR;
        int node_id;

        if (lines[i][0] == '$')
            return i;

        strcpy(linebuf, lines[i]);
        tok = strtok(linebuf, " ");

        if (isdigit(tok[0])) {
            size = atoi(tok);
            type = NT_FILE;
        }

        tok = strtok(NULL, " ");
        node_id = fs_push(tok, size, type);

        if (node_id != 0)
            cwd_push(node_id);

    }

    return lines_len;
}

// gets pointer to the CWD (current working directory), which is root if
// the stack is empty.
struct fsnode *
get_cwd(void)
{
    if (cwdlen == 0)
        return fsroot;

    return get_node(cwd[cwdlen - 1]);
}

// examines the names of the child nodes (children[]) and grabs the ID
// of the one that matches the given name. If no match is found, returns
// 0. Otherwise, the found directory's ID is pushed to the stack,
// becoming the new CWD, and 1 is returned to indicate success
int
cd_to_child(char const *name)
{
    struct fsnode *loc = get_cwd();

    for (int i = 0; i < loc->nchildren; ++i) {
        int child_id = loc->children[i];
        struct fsnode *child = get_node(child_id);

        if (strcmp(child->name, name) == 0) {
            cwd[cwdlen++] = child_id;
            return 1;
        }
    }

    return 0;
}

// wipes the stack, setting CWD to root
void
cd_to_root(void)
{
    cwdlen = 0;
}

// pops the stack, returning CWD to the parent
void
cd_to_parent(void)
{
    --cwdlen;
}

// parses cd command
void
exec_cd(char const *tok)
{
    if (tok[0] == '/')
        cd_to_root();

    else if (memcmp(tok, "..", 2) == 0)
        cd_to_parent();

    else
        cd_to_child(tok);
}

void
print_filesystem_detail(void)
{
    for (size_t i = 0; i < FSLIMIT; ++i) {
        if (fsroot[i].type == NT_EMPTY)
            break;

        printf("ID %lu '%s' (%s, ", i, fsroot[i].name, fsnodetype_str[fsroot[i].type]);

        if (fsroot[i].type == NT_FILE)
            printf("%lu bytes)\n", fsroot[i].size);

        else {
            printf("%u children)\n", fsroot[i].nchildren);

            for (int j = 0; j < fsroot[i].nchildren; ++j) {
                int child_id = fsroot[i].children[j];

                printf("  %u: ID %u '%s'\n", j + 1, child_id, fsroot[child_id].name);
            }
        }
    }
}

void
load_filesystem(void)
{
    static char linebuf[64];

    for (size_t i = 0; i < lines_len;) {
        char *tok;

        strcpy(linebuf, lines[i] + 2);
        tok = strtok(linebuf, " ");

        if (strcmp(tok, "cd") == 0) {
            exec_cd(strtok(NULL, " "));
            ++i;
            continue;
        }

        if (strcmp(tok, "ls") == 0)
            i = ls_directory(i + 1);
    }
}

size_t
collect_size(int id)
{
    size_t total = 0;
    struct fsnode *selnode = get_node(id);

    if (selnode->type == NT_EMPTY)
        return 0;

    if (selnode->type == NT_FILE)
        return selnode->size;

    for (int i = 0; i < selnode->nchildren; ++i)
        total += collect_size(selnode->children[i]);

    return total;
}
