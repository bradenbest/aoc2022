#include <stdio.h>
#include <stdlib.h>

#include "get_line.h"
#include "strntol.h"

#define NELVES_MAX 1000

static long elves[NELVES_MAX];
static size_t nelves = 0;

static inline long
read_elf_calories(FILE *fp)
{
    long total = 0;
    char buffer[12];
    size_t nread;

    while ((nread = get_line_from(buffer, 12, fp)) > 0)
        total += strntol(buffer, nread, 10);

    return total;
}

static int
compare(void const *va, void const *vb)
{
    long const *a = va;
    long const *b = vb;

    return *b - *a;
}

static inline void
setup(void)
{
    FILE *fp = fopen("input", "r");

    while (!feof(fp)) {
        if (nelves == NELVES_MAX)
            printf("fatal: too many elves\n");

        elves[nelves++] = read_elf_calories(fp);
    }

    fclose(fp);
    qsort(elves, nelves, sizeof *elves, compare);
}

static inline void
part1(void)
{
    printf("Part 1: Largest calorie count = %lu\n", elves[0]);
}

static inline void
part2(void)
{
    printf("Part 2: Top 3 = %lu, %lu, %lu\n", elves[0], elves[1], elves[2]);
    printf("Sum: %lu\n", elves[0] + elves[1] + elves[2]);
}

int
main(void)
{
    setup();
    part1();
    part2();
    return 0;
}
