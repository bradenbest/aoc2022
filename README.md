My solutions for AOC 2022. I won't necessarily do all of them.

Entries are sorted by day. Each directory has a number of files. Generally:

* part1.c - code for part 1
* part2.c - code for part 2
* common.h - common code shared by part1 and part2
* desc - description of challenge (copied from site)
* input - input
* testinput - example input given in desc, if used
* Makefile - makefile

There are also a lib/ and util/ directory for header libraries and tools. For example, util/preproc preprocesses input
and turns it into a header that can be compiled into the program. preproc.mk sets up the necessary make build
instructions to make it work
