#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>

#include "common.h"

int
pair_overlaps(struct range_pair const *pair)
{
    struct range const *r1 = &(pair->r1);
    struct range const *r2 = &(pair->r2);
    int r1_outside = r1->start > r2->end || r1->end < r2->start;
    int r2_outside = r2->start > r1->end || r2->end < r1->start;

    return !(r1_outside || r2_outside);
}

int
main(void)
{
    FILE *fp = fopen("input", "r");

    if (fp == NULL)
        return 1;

    printf("Total overlap incidents = %u\n", get_overlaps(fp, pair_overlaps));
    fclose(fp);
    return 0;
}
