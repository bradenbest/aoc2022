struct range {
    uint8_t start;
    uint8_t end;
};

struct range_pair {
    struct range r1;
    struct range r2;
};

typedef int (*pairfn)(struct range_pair const *);

static inline void
parse_line(char *line, struct range_pair *pair)
{
    static char const *delim = "-,\n";
    uint8_t *targets[] = {
        &(pair->r1.start),
        &(pair->r1.end),
        &(pair->r2.start),
        &(pair->r2.end),
    };
    char *tok = strtok(line, delim);

    for (int i = 0; i < 4; ++i) {
        *(targets[i]) = atoi(tok);
        tok = strtok(NULL, delim);
    }
}

static inline int
get_overlaps(FILE *fp, pairfn callback)
{
    struct range_pair temp;
    char line[16];
    int total = 0;

    while (fgets(line, 16, fp) != NULL) {
        parse_line(line, &temp);
        total += callback(&temp);
    }

    return total;
}

