#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>

#include "common.h"

int
range_contains_range(struct range const *container, struct range const *containee)
{
    return containee->start >= container->start &&
           containee->end <= container->end;
}

int
pair_range_contains_range(struct range_pair const *pair)
{
    return range_contains_range(&(pair->r1), &(pair->r2)) ||
           range_contains_range(&(pair->r2), &(pair->r1));
}

int
main(void)
{
    FILE *fp = fopen("input", "r");

    if (fp == NULL)
        return 1;

    printf("Total range-inside-range incidents = %u\n", get_overlaps(fp, pair_range_contains_range));
    fclose(fp);
    return 0;
}
