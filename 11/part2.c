#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>

#include "common.h"

static inline u64
get_set_product(void)
{
    u64 out = monkeys[0].test;

    for (int i = 1; i < NMONKEYS; ++i)
        out *= monkeys[i].test;

    return out;
}

static inline void
exec_monkey_turn(struct monkey *selmon)
{
    u64 product = get_set_product();

    for (int i = 0; i < selmon->items_len; ++i) {
        u64 worry = selmon->items[i];
        int target;

        worry = selmon->operation(worry, selmon->operation_arg);
        worry %= product;
        target = (worry % selmon->test == 0) ? selmon->iftrue : selmon->iffalse;
        monkey_push(monkeys + target, worry);
    }

    selmon->inspections += selmon->items_len;
    selmon->items_len = 0;
}

static inline void
exec_round(void)
{
    for (int i = 0; i < NMONKEYS; ++i)
        exec_monkey_turn(monkeys + i);
}

int
main(void)
{
    u64 activity[NMONKEYS];
    u64 expected = 21816744824UL; // obtained from a known working solution
    u64 actual;

    load_input();
    print_monkeys();

    for (int i = 0; i < 10000; ++i)
        exec_round();

    print_monkeys();

    for (int i = 0; i < NMONKEYS; ++i)
        activity[i] = monkeys[i].inspections;

    qsort(activity, NMONKEYS, sizeof *activity, intcmp_desc);
    actual = activity[0] * activity[1];
    printf("Monkey business = %lu * %lu = %lu\n", activity[0], activity[1], actual);

    if (actual != expected)
        printf("FAIL. Expected: %lu\n", expected);

    return 0;
}
