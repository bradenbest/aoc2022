#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>

#include "common.h"

static inline void
exec_monkey_turn(struct monkey *selmon)
{
    for (int i = 0; i < selmon->items_len; ++i) {
        int worry = selmon->items[i];
        int target;

        worry = selmon->operation(worry, selmon->operation_arg);
        worry /= 3;
        target = (worry % selmon->test == 0) ? selmon->iftrue : selmon->iffalse;
        monkey_push(monkeys + target, worry);
    }

    selmon->inspections += selmon->items_len;
    selmon->items_len = 0;
}

static inline void
exec_round(void)
{
    for (int i = 0; i < NMONKEYS; ++i)
        exec_monkey_turn(monkeys + i);
}

int
main(void)
{
    int activity[NMONKEYS];

    load_input();
    print_monkeys();

    for (int i = 0; i < 20; ++i)
        exec_round();

    print_monkeys();

    for (int i = 0; i < NMONKEYS; ++i)
        activity[i] = monkeys[i].inspections;

    qsort(activity, NMONKEYS, sizeof *activity, intcmp_desc);
    printf("Monkey business = %u * %u = %u\n", activity[0], activity[1], activity[0] * activity[1]);

    return 0;
}
