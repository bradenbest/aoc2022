#include <assert.h>
#include <limits.h>

#include "input.h"
#define NMONKEYS 8
#define NITEMS 36

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef u64 (*transform_fn)(u64 old, u64 arg);

struct monkey {
    u64          items[NITEMS];
    u8           items_len;
    transform_fn operation;
    u64          operation_arg;
    u8           test;
    u8           iftrue;
    u8           iffalse;
    u32          inspections;
};

static struct monkey monkeys[NMONKEYS];

static u64
tf_add(u64 old, u64 arg)
{
    assert (UINT64_MAX - arg >= old);
    return old + arg;
}

static u64
tf_mul(u64 old, u64 arg)
{
    assert (UINT64_MAX / arg >= old);
    return old * arg;
}

static u64
tf_square(u64 old, u64 unused)
{
    (void) unused;
    assert (UINT64_MAX / old >= old);
    return old * old;
}

static inline void
monkey_push(struct monkey *selmon, u64 value)
{
    selmon->items[selmon->items_len++] = value;
}

static inline void
parse_starting_items(char const **lines_offset, struct monkey *selmon)
{
    char const *segment = lines_offset[1] + strlen("  Starting items: ");
    char linebuf[512];
    char *tok;

    strcpy(linebuf, segment);
    tok = strtok(linebuf, ", ");

    while (tok != NULL) {
        monkey_push(selmon, atoi(tok));
        tok = strtok(NULL, ", ");
    }
}

static inline void
parse_operation(char const **lines_offset, struct monkey *selmon)
{
    char const *segment = lines_offset[2] + strlen("  Operation: new = old ");
    int self_arg = strcmp(segment + 2, "old") == 0;

    if (segment[0] == '+') {
        selmon->operation = tf_add;
        selmon->operation_arg = atoi(segment + 2);

        if (self_arg) {
            selmon->operation = tf_mul;
            selmon->operation_arg = 2;
        }

        return;
    }

    if (segment[0] == '*') {
        selmon->operation = tf_mul;
        selmon->operation_arg = atoi(segment + 2);

        if (self_arg)
            selmon->operation = tf_square;
    }
}

static inline void
parse_test(char const **lines_offset, struct monkey *selmon)
{
    char const *testsegment = lines_offset[3] + strlen("  Test: divisible by ");
    char const *truesegment = lines_offset[4] + strlen("    If true: throw to monkey ");
    char const *falsesegment = lines_offset[5] + strlen("    If false: throw to monkey ");

    selmon->test = atoi(testsegment);
    selmon->iftrue = atoi(truesegment);
    selmon->iffalse = atoi(falsesegment);
}

static inline void
load_input(void)
{
    struct monkey *selmon = monkeys;

    for (size_t i = 0; i < lines_len; i += 7) {
        parse_starting_items(lines + i, selmon);
        parse_operation(lines + i, selmon);
        parse_test(lines + i, selmon);
        ++selmon;
    }
}

static inline char const *
get_operation_str(transform_fn fn)
{
    if (fn == tf_add)
        return "add";

    if (fn == tf_mul)
        return "mul";

    if (fn == tf_square)
        return "square";

    return "<illegal>";
}

static inline void
print_monkeys(void)
{
    for (int i = 0; i < NMONKEYS; ++i) {
        printf("Monkey %u: items_len %u, operation %s(%lu), test /%u ? %u : %u\n  ",
                i, monkeys[i].items_len, get_operation_str(monkeys[i].operation),
                monkeys[i].operation_arg, monkeys[i].test,
                monkeys[i].iftrue, monkeys[i].iffalse);

        for (int j = 0; j < monkeys[i].items_len; ++j)
            printf("%lu ", monkeys[i].items[j]);

        printf("\n");
    }
}

static int
intcmp_desc(void const *a, void const *b)
{
    return *(int *)b - *(int *)a;
}
