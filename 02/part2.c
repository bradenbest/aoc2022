#include <stdio.h>
#include <string.h>

#include "common.h"

//#define DEBUG

#ifdef DEBUG
static char const *play_str[] = { "Rock", "Paper", "Scissors", };
static char const *outcome_str[] = { "Lose", "Draw", "Win", };
#endif

static inline int
get_play(int ch, int playerid)
{
    return ch - ("AX"[playerid]);
}

static inline int
get_play_shape(int opponent, int outcome)
{
    static int const lut[3][3] = {
        /* want lose, draw, win*/
        { PT_SCISSORS, PT_ROCK,     PT_PAPER    }, /* opponent rock */
        { PT_ROCK,     PT_PAPER,    PT_SCISSORS }, /* paper */
        { PT_PAPER,    PT_SCISSORS, PT_ROCK     }, /* scissors */
    };
    return lut[opponent][outcome];
}

int
main(void)
{
    FILE *fp = fopen("input", "r");
    char buffer[4];
    size_t nread;
    long score = 0;

    while ((nread = fread(buffer, sizeof *buffer, 4, fp)) >= 3) {
        int opponent = get_play(buffer[0], PID_OPPONENT);
        int player = get_play(buffer[2], PID_PLAYER);
        int play_shape = get_play_shape(opponent, player);

        score += 3 * player + play_shape + 1;
#ifdef DEBUG
        printf("%.*s: %s (%s) player plays %s\n",
                3, buffer, play_str[opponent], outcome_str[player], play_str[play_shape]);
#endif
    }

    printf("Total score: %lu\n", score);
    fclose(fp);
    return 0;
}
