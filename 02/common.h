enum game_result { GR_LOSE, GR_DRAW, GR_WIN, };
enum play_type { PT_ROCK, PT_PAPER, PT_SCISSORS, };
enum player_id { PID_OPPONENT, PID_PLAYER, };
