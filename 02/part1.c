#include <stdio.h>
#include <string.h>

#include "common.h"

static inline int
game_result(int opponent_ch, int player_ch)
{
    static int const lut[3][3] = {
        /* player rock, paper, scissors */
        { GR_DRAW, GR_WIN,  GR_LOSE }, /* opponent rock */
        { GR_LOSE, GR_DRAW, GR_WIN  }, /* paper */
        { GR_WIN,  GR_LOSE, GR_DRAW }, /* scissors */
    };
    int opponent = opponent_ch - 'A';
    int player = player_ch - 'X';

    return lut[opponent][player];
}

static inline int
shape_score(int player_ch)
{
    return (player_ch - 'X') + 1;
}

int
main(void)
{
    long score = 0;
    FILE *fp = fopen("input", "r");
    char buffer[4];
    size_t nread;

    while ((nread = fread(buffer, sizeof *buffer, 4, fp)) >= 3) {
        int opponent = buffer[0];
        int player = buffer[2];
        int res = game_result(opponent, player);

        score += 3 * res + shape_score(player);
    }

    printf("Total score: %lu\n", score);
    fclose(fp);
    return 0;
}
