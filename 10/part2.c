#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "common.h"

#define IABS(x) \
    ( ((x) < 0) ? (-(x)) : (x) )

static struct cpustate dummyret;

static inline void
draw_cpustate(void)
{
    int cycle = cpustate.cycleno;
    int adjcycle = ((cycle - 1) % 40) + 1;
    int diff = IABS(adjcycle - cpustate.xregister);

    if (diff < 2)
        putchar('#');
    else
        putchar('.');

    if (cycle % 40 == 0)
        putchar('\n');
}

static struct cpustate
instr_nop(int unused)
{
    (void) unused;

    draw_cpustate();
    cpustate.cycleno += 1;

    return dummyret;
}

/* 1. The instructions in this part are VERY unclear. The example
 *    printout heavily implies that drawing is done before the x
 *    register is actually modified. I found out the actual order it
 *    wanted on a hunch based on the mangled output I was getting.
 *    From the assignment:
 *
 *        During cycle 21: CRT draws pixel in position 20
 *        End of cycle 21: finish executing addx -1
 *
 *    Do note that the positions start from 0 while the cycles start
 *    from 1. So it's talking about drawing BEFORE adding, while the
 *    only way I was able to match their output was by doing them in the
 *    exact opposite order. The example is wrong and misleading.
 */
static struct cpustate
instr_add(int arg)
{
    draw_cpustate();
    cpustate.cycleno += 1;

    cpustate.xregister += arg; // 1
    draw_cpustate();
    cpustate.cycleno += 1;

    return dummyret;
}

int
main(void)
{
    load_input();

    for (int i = 0; i < INPUT_LENGTH; ++i)
        instr_handler[instructions[i].opcode](instructions[i].arg);

    while (cpustate.cycleno < 240) {
        cpustate.cycleno += 1;
        draw_cpustate();
    }

    return 0;
}
