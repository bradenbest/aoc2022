#include "input.h"

#define SIGNAL_STRENGTH(state) \
    ( (state).cycleno * (state).xregister )

#define ARBITRARY_TEST(n) \
    ( ((n) == 20) || (((n) - 20) % 40 == 0) )

#define INPUT_LENGTH 143

enum opcodes {
    OP_NOP,
    OP_ADD,
};

struct cpuinstruction {
    int opcode;
    int arg;
};

struct cpustate {
    int cycleno;
    int xregister;
};

typedef struct cpustate (*instrfn)(int arg);

static struct cpustate  instr_nop  (int unused);
static struct cpustate  instr_add  (int arg);

static instrfn const instr_handler[] = { instr_nop, instr_add };
static struct cpustate cpustate = {1, 1};
static struct cpuinstruction instructions[INPUT_LENGTH];

static inline void
load_input(void)
{
    for (size_t i = 0; i < lines_len; ++i) {
        struct cpuinstruction *selinstr = instructions + i;
        char const *argstr = strchr(lines[i], ' ');

        if (lines[i][0] == 'a') {
            selinstr->opcode = OP_ADD;
            selinstr->arg = atoi(argstr);
        }

        else
            selinstr->opcode = OP_NOP;
    }
}

