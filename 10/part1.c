#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "common.h"

static char const *instr_str[] = { "noop", "addx" };

static struct cpustate
instr_nop(int unused)
{
    (void) unused;
    int cycleno = cpustate.cycleno;

    cpustate.cycleno += 1;

    if (ARBITRARY_TEST(cycleno))
        return (struct cpustate){cycleno, cpustate.xregister};

    return (struct cpustate){0, 0};
}

static struct cpustate
instr_add(int arg)
{
    int cycleno = cpustate.cycleno;

    cpustate.xregister += arg;
    cpustate.cycleno += 2;

    if (ARBITRARY_TEST(cycleno))
        return (struct cpustate){cycleno, cpustate.xregister - arg};

    if (ARBITRARY_TEST(cycleno + 1))
        return (struct cpustate){cycleno + 1, cpustate.xregister - arg};

    return (struct cpustate){0, 0};
}

int
main(void)
{
    int total = 0;

    load_input();

    for (int i = 0; i < INPUT_LENGTH; ++i) {
        struct cpuinstruction const *selinstr = instructions + i;
        struct cpustate interim_state = instr_handler[selinstr->opcode](selinstr->arg);

        if (interim_state.cycleno != 0) {
            printf("%s %i / Cycle %u,  X %i, strength %i\n",
                    instr_str[selinstr->opcode],
                    selinstr->arg,
                    interim_state.cycleno,
                    interim_state.xregister,
                    SIGNAL_STRENGTH(interim_state) );
            total += SIGNAL_STRENGTH(interim_state);
        }
    }

    printf("total %u\n", total);
}
