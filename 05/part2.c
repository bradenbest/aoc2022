#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "common.h"

static inline void
run_simulation(size_t lineno)
{
    static char temp_stack[256];
    size_t tslen = 0;

    for (size_t i = lineno; i < lines_len; ++i) {
        struct mvinstr mv = parse_move(lines[i]);

        while (mv.amount > 0) {
            char copied = stack_pop(mv.srcid);

            temp_stack[tslen++] = copied;
            --mv.amount;
        }

        while(tslen > 0)
            stack_push(mv.destid, temp_stack[--tslen]);
    }
}
