#include "input.h"

struct mvinstr {
    int amount;
    int srcid;
    int destid;
};

static inline void   stack_push          (int stackid, char value);
static inline char   stack_pop           (int stackid);
static inline void   process_stack       (size_t start_lineno, int stackid);
static inline void   process_stacks      (size_t lineno);
static inline struct mvinstr parse_move  (char const *line);
static inline void   print_stacks        (void);
static inline size_t index_of_empty_line (void);

static inline void run_simulation (size_t lineno);

static char stacks[9][512];
static size_t stacklens[9];

static inline void
stack_push(int stackid, char value)
{
    char *selstack = stacks[stackid];
    size_t *len = stacklens + stackid;

    selstack[(*len)++] = value;
}

static inline char
stack_pop(int stackid)
{
    char *selstack = stacks[stackid];
    size_t *len = stacklens + stackid;

    return selstack[--(*len)];
}

static inline void
process_stack(size_t start_lineno, int stackid)
{
    int xoffset = stackid * 4 + 1;

    for (int cur_lineno = start_lineno; cur_lineno >= 0; --cur_lineno) {
        char ch = lines[cur_lineno][xoffset];

        if (ch == ' ')
            return;

        stack_push(stackid, ch);
    }

}

static inline void
process_stacks(size_t lineno)
{
    for (int i = 0; i < 9; ++i)
        process_stack(lineno, i);
}

static inline struct mvinstr
parse_move(char const *line)
{
    char linecp[32];
    char *tok;
    struct mvinstr out;

    strcpy(linecp, line + 5);
    tok = strtok(linecp, " ");
    out.amount = atoi(tok);

    strtok(NULL, " ");
    tok = strtok(NULL, " ");
    out.srcid = atoi(tok) - 1;

    strtok(NULL, " ");
    tok = strtok(NULL, " ");
    out.destid = atoi(tok) - 1;

    return out;
}

static inline void
print_stacks(void)
{
    puts("stacks:");

    for (int i = 0; i < 9; ++i)
        printf("%.*s\n", (int)(stacklens[i]), stacks[i]);

    puts("");
}

static inline size_t
index_of_empty_line(void)
{
    size_t i;

    for (i = 0; lines[i][0] != '\0' && i < lines_len; ++i)
        ;

    return i;
}

int
main(void)
{
    size_t i = index_of_empty_line();

    process_stacks(i - 2);
    print_stacks();
    run_simulation(i + 1);
    print_stacks();

    printf("String: ");

    for (size_t j = 0; j < 9; ++j)
        putchar(stack_pop(j));

    putchar('\n');
    return 0;
}
