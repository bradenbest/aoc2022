#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "common.h"

static inline void
run_simulation(size_t lineno)
{
    for (size_t i = lineno; i < lines_len; ++i) {
        struct mvinstr mv = parse_move(lines[i]);

        while (mv.amount > 0) {
            char copied = stack_pop(mv.srcid);

            stack_push(mv.destid, copied);
            --mv.amount;
        }
    }
}
