CFLAGS = -Wall -Wextra -I../lib -O3
TARGETS = input.h part1 part2

all: $(TARGETS)

clean_all:
	rm -f $(TARGETS)
