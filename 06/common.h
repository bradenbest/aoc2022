static char queue[QUEUESZ];
static int queuelen;

// pushes character into queue and shuffles if needed
char
queue_push(char ch)
{
    static char temp[QUEUESZ - 1];

    if (queuelen < QUEUESZ)
        return queue[queuelen++] = ch;

    memcpy(temp, queue + 1, QUEUESZ - 1);
    memcpy(queue, temp, QUEUESZ - 1);
    return queue[QUEUESZ - 1] = ch;
}

// checks each character against everything after, n^2/2
// incomplete queue is considered automatically not unique
int
queue_uniq(void)
{
    if (queuelen < QUEUESZ)
        return 0;

    for (int i = 0; i < QUEUESZ; ++i)
        if (memchr(queue + i + 1, queue[i], QUEUESZ - (i + 1)) != NULL)
            return 0;

    return 1;
}

int
main(void)
{
    FILE *fp = fopen("input", "r");
    int ch;
    int nread = 0;

    if (fp == NULL)
        return 1;

    while ((ch = fgetc(fp)) != EOF) {
        queue_push(ch);
        ++nread;

        if (queue_uniq()) {
            printf("Chars read until marker: %u\n", nread);
            break;
        }
    }

    fclose(fp);
    return 0;
}
