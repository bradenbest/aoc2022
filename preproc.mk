# for preprocessing input

input.h: ../util/preproc
	$^ < input > $@

clean_util:
	cd ../util && $(MAKE) clean

.PHONY: clean_util
